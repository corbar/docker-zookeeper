FROM ubuntu:16.04
MAINTAINER Cornel


ENV DEBIAN_FRONTEND noninteractive

# Install Kafka, Zookeeper and other needed things
RUN echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" > /etc/apt/sources.list.d/webupd8team-java.list \
    && echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu xenial main" >> /etc/apt/sources.list.d/webupd8team-java.list \
    && apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886 \
    && apt-get -y update \
    && echo debconf shared/accepted-oracle-license-v1-1 select true | /usr/bin/debconf-set-selections \
    && apt-get -y install oracle-java8-installer oracle-java8-set-default \
    && apt-get -y install zookeeper supervisor \
    && rm -rf /var/lib/apt/lists/* \
    && apt-get clean 

# Supervisor config
COPY supervisor/zookeeper.conf /etc/supervisor/conf.d/

# 2181 is zookeeper
EXPOSE 2181

CMD ["supervisord", "-n"]

